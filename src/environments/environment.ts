// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyB42wpTt6FyemLINg6QkEf7Wg_sdwsBAOA",
  authDomain: "angulartest-30f0c.firebaseapp.com",
  databaseURL: "https://angulartest-30f0c.firebaseio.com",
  projectId: "angulartest-30f0c",
  storageBucket: "angulartest-30f0c.appspot.com",
  messagingSenderId: "1020462791400",
  appId: "1:1020462791400:web:24d59ba3e13d6561e3a75d",
  measurementId: "G-ZG4MSEHRFK"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
